package com.matrix.yukun.matrix.gaia_module.util;

/**
 * Created by yukun on 16-11-25.
 */
public class EventPosition {
    public int position;
    public double loadtime;

    public EventPosition(int position, double loadtime) {
        this.position = position;
        this.loadtime = loadtime;
    }
}
