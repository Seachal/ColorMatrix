package com.matrix.yukun.matrix.gaia_module.net;

public class Api {
    public static String BASE_URL="https://gaiamount.com/web";
    public static String COVER_PREFIX = "https://qw.gaiamount.com/";
    public static String BANNER="/cms/home/getShuffling";
    public static String RECOMEND="/cms/home/recommend";
    public static String SEARCHWORK="/works/search";
    public static String SEARCHMATRIL="/material/getList";
    public static String PERSONURL="/creator/person/getList";
    public static String LOGINURL="/account/_login";
    public static String WORKVIDEOURL="/works/details";
    public static String MATERCIALVIDEOURL="/material/details";
    public static String WORKRECOND="/works/recommendedWorks";
    public static String MATRERIALRECOND="/material/recommend";
    public static String TSURL ="https://qv.gaiamount.com" ;
}
