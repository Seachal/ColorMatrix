package com.matrix.yukun.matrix.gaia_module.adapter.create_person;

import java.util.ArrayList;

/**
 * Created by yukun on 16-7-28.
 */
public class WorksInfo {
    private ArrayList<CoverImgInfo> coverImgInfos;

    public ArrayList<CoverImgInfo> getCoverImgInfos() {
        return coverImgInfos;
    }

    public void setCoverImgInfos(ArrayList<CoverImgInfo> coverImgInfos) {
        this.coverImgInfos = coverImgInfos;
    }
}
